"use strict";

let smile = newSmile();

// Выбранный тип теста: СМИЛ или ММИЛ.
let chosenTest;
// Выбран ли женский пол?
let isFemaleGender;

document.addEventListener("DOMContentLoaded", initializePage);

function initializePage() {
    isFemaleGender = document.getElementById("radio-gender-female").checked;
    if (document.getElementById("radio-test-type-smil").checked) chosenTest = smile.smil;
    else chosenTest = smile.mmil;

    // переключатель типа теста
    document.getElementById("radio-test-type-smil").addEventListener("change", setTestTypeSMIL);
    document.getElementById("radio-test-type-mmil").addEventListener("change", setTestTypeMMIL);
    // переключатель пола
    document.getElementById("radio-gender-male").addEventListener("change", setMaleGender);
    document.getElementById("radio-gender-female").addEventListener("change", setFemaleGender);
    // поля ввода диапазона результата (значения, не попавшие в диапазон выделяются красным цветом)
    document.getElementById("input-min-result-value").addEventListener("change", fillResults);
    document.getElementById("input-max-result-value").addEventListener("change", fillResults);
    // переключатель видимости варианта "не знаю"
    document.getElementById("checkbox-hide-not-sure").addEventListener("change", changeHidingNotSure);
    // кнопка, копирующая текущие ответы пользователя в формате JSON
    document.getElementById("input-copy-button").addEventListener("click", copyOutput);
    // кнопка, загружающая вставленные в поле ввода ответы пользователя в формате JSON
    document.getElementById("input-load-button").addEventListener("click", loadInput);
    // поле ввода для загрузки ответов в формате JSON
    document.getElementById("io-input").addEventListener("input", checkInput);

    fillStatementsWith();
}

let anotherTestAnswers;
function setTestTypeSMIL() {
    let answers = anotherTestAnswers;
    anotherTestAnswers = getAnswers();

    chosenTest = smile.smil;
    resetStatementsAndResults();
    fillStatementsWith(answers);
}
function setTestTypeMMIL() {
    let answers = anotherTestAnswers;
    anotherTestAnswers = getAnswers();

    chosenTest = smile.mmil;
    resetStatementsAndResults();
    fillStatementsWith(answers);
}

function setFemaleGender() {
    isFemaleGender = true;
    fillStatementsWith(getAnswers());
}
function setMaleGender() {
    isFemaleGender = false;
    fillStatementsWith(getAnswers());
}

function checkInput(event) {
    let hasError = false;
    try {
        JSON.parse(event.target.value);
    } catch (error) {
        hasError = true;
    } finally {
        document.getElementById("input-load-button").disabled = hasError;
    }
}

function loadInput() {
    let inputValue = document.getElementById("io-input").value;
    let answers = JSON.parse(inputValue);
    fillStatementsWith(answers);
}

function resetStatementsAndResults() {
    let resultsNode = document.getElementById("results");
    while (resultsNode.hasChildNodes()) resultsNode.removeChild(resultsNode.lastChild);
    let additionalResultsNode = document.getElementById("additional-results");
    while (additionalResultsNode.hasChildNodes()) additionalResultsNode.removeChild(additionalResultsNode.lastChild);
    let statementsNode = document.getElementById("statements");
    while (statementsNode.hasChildNodes()) statementsNode.removeChild(statementsNode.lastChild);
}

function copyOutput() {
    let output = document.getElementById("io-input");
    output.select();
    output.setSelectionRange(0, 99999);
    document.execCommand("copy");
}

function changeHidingNotSure(event) {
    for (let i = 1; i < chosenTest.statements.length; i++) document.getElementById("span-not-sure-" + i.toString()).hidden = event.target.checked;
}

function fillStatementsWith(answers = null) {
    let statementsNode = document.getElementById("statements");
    for (let i = 1; i < chosenTest.statements.length; i++) {
        let isChecked = null;
        if (answers !== null) {
            if (answers.yes.includes(i)) isChecked = true;
            else if (answers.no.includes(i)) isChecked = false;
        }
        fillStatementOptions(statementsNode, chosenTest.statements[i].getValue(isFemaleGender), i, isChecked);
    }
    fillResults();
}

function fillStatementOptions(node, statement, i, isChecked = null) {
    let index = i.toString();
    let element = document.getElementById("statement-text-" + index);

    if (element !== null) element.innerText = index + ". " + statement; // обновление текста уже существующего утверждения
    else {
        // создание нового элемента для утверждения
        let element = document.createElement("div");
        element.id = "statement-" + index;
        element.classList.add("column");

        element.innerHTML = `<p id="statement-text-` + index + `">` + index + ". " + statement + `</p>` +
        `<p class="row"><span><input type="radio" id="radio-yes-` + index + `" name="radio-` + index + `"><label for="radio-yes-` + index + `">Да</label></span>` +
        `<span><input type="radio" id="radio-no-` + index + `" name="radio-` + index + `"><label for="radio-no-` + index + `">Нет</label></span>` +
        `<span id="span-not-sure-` + index + `"><input type="radio" id="radio-not-sure-` + index + `" name="radio-` + index + `" checked><label for="radio-not-sure-` + index + `">Не знаю</label></span></p>`;

        node.insertBefore(element, null);

        document.getElementById("radio-yes-" + index).addEventListener("change", fillResults);
        document.getElementById("radio-no-" + index).addEventListener("change", fillResults);
        document.getElementById("radio-not-sure-" + index).addEventListener("change", fillResults);
    }

    if (isChecked !== null) {
        if (isChecked) document.getElementById("radio-yes-" + index).checked = true;
        else if (!isChecked) document.getElementById("radio-no-" + index).checked = true;
    } else document.getElementById("radio-not-sure-" + index).checked = true;
}

function fillResults() {
    let answers = getAnswers();
    // получение результатов
    let results = smile.getResults(answers, isFemaleGender, chosenTest.scales);
    // заполнение основных результатов (отображаются справа)
    let resultsNode = document.getElementById("results");
    for (let i = 0; i < chosenTest.scales.length; i++) fillScaleResult(resultsNode, chosenTest.scales[i], results[i], i.toString());
    // заполнение ответов по шкалам
    let statementResultsNode = document.getElementById("statement-results");
    for (let i = 0; i < chosenTest.scales.length; i++) fillScaleStatementResult(statementResultsNode, chosenTest.scales[i], results[i], i.toString());

    // заполнение дополнительных результатов (отображаются внизу)
    let additionalResults = smile.getResults(answers, isFemaleGender, chosenTest.additionalScales);
    let additionalResultsNode = document.getElementById("additional-results");

    if (additionalResults.length === 0) additionalResultsNode.innerText = "Отсутствуют";
    else additionalResultsNode.innerText = "";

    for (let i = 0; i < chosenTest.additionalScales.length; i++) fillScaleResult(additionalResultsNode, chosenTest.additionalScales[i], additionalResults[i], i.toString(), true);
    // ответы в формате JSON (для копирования, сохранения)
    document.getElementById("io-input").value = JSON.stringify(answers);
}


function fillScaleStatementResult(node, scale, result, index) {
    // получение содержимого элемента
    let innerHTML =  `<span>Да: ` + result.yes.join(", ") + `</span>` +
        `<span>Нет: ` + result.no.join(", ") + `</span>`;

    let element = document.getElementById("statement-result-value-" + index);
    if (element !== null) element.innerHTML = innerHTML; // обновление уже существующего результата шкалы
    else {
        // для создания нового элемента результата шкалы
        let element = document.createElement("p");
        element.id = "statement-result-" + index;
        element.classList.add("left-column");
        let designation = (scale.designation !== "") ? scale.designation + ". " : "";
        element.innerHTML =  `<span>` +  designation + scale.name + `</span>` +
            `<span class="left-column" id="statement-result-value-` + index + `">` + innerHTML + `</span>`;
        node.insertBefore(element, null);
    }
}

function fillScaleResult(node, scale, result, index, isAdditional= false) {
    let additionalSuffix = (isAdditional) ? "-additional" : "";
    // получение содержимого элемента
    let innerHTML = `<span title="T-значение">` + result.value.toString() + `</span>` +
        ` <span title="Сырое значение">(` + result.correctedValue.toString() + `)</span>`;

    let element = document.getElementById("result-value-" + index + additionalSuffix);
    if (element !== null) element.innerHTML = innerHTML; // обновление уже существующего результата шкалы
    else {
        // для создания нового элемента результата шкалы
        let element = document.createElement("p");
        element.id = "result-" + index + additionalSuffix;
        if (!isAdditional) element.classList.add("result");
        let designation = (scale.designation !== "") ? scale.designation + ". " : "";
        element.innerHTML = designation + scale.name + `: <span class="result-value" id="result-value-` + index + additionalSuffix + `">` + innerHTML + `</span>`;
        node.insertBefore(element, null);
    }

    if (!isAdditional) {
        // выделение результатов, которые не попали в заданный диапазон
        let min = document.getElementById("input-min-result-value").value;
        let max = document.getElementById("input-max-result-value").value;
        if (result.value < min || result.value > max) document.getElementById("result-" + index).classList.add("result-bad");
        else document.getElementById("result-" + index).classList.remove("result-bad");
    }
}

function getAnswers() {
    let answers = {yes: [], no: []};
    for (let i = 1; i < chosenTest.statements.length; i++) {
        let index = i.toString();
        let radioYes = document.getElementById("radio-yes-" + index);
        let radioNo = document.getElementById("radio-no-" + index);

        if (radioYes.checked) answers.yes.push(i);
        else if (radioNo.checked) answers.no.push(i);
    }
    return answers;
}